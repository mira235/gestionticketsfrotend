import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import { user } from '../_services/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  constructor( private userservice: UserService,private router:Router) { }
email:string
username:string
id:any
user:user
  ngOnInit() {
  }
resetpassword(username:string){
  this.userservice.getuserByusernamee(username).subscribe(res => {

console.log(res)
this.user=res
this.userservice.getIdByusername(username).subscribe(id => {

  console.log(id)

console.log(this.user)
this.userservice.updateUser(this.user,id).subscribe(data => {

  console.log(data)
  this.router.navigate(["login"])
})
})
  })

}
}
