import { Component, OnInit } from '@angular/core';
import * as Chart from 'chart.js';
import { TicketServiceService } from '../ticket-service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent  {  title = 'angular8chartjs';
canvas: any;
st:any ;
st2:any;
st3:any;
st4:any;
ctx: any;
constructor(private ticketService:TicketServiceService ) {}


ngAfterViewInit() {
 this.ticketService.getNombreTiketsEnAttente() .subscribe(data=>{
  console.log(data+"id")

this.st=data
  console.log(data)
  this.ticketService.getNombreTiketsFerme() .subscribe(data2=>{
    console.log(data2+"id")
  
  this.st2=data2
    console.log(data2)
    this.ticketService.getNombreTiketsEncour() .subscribe(data3=>{
      
    
    this.st3=data3
      console.log(data3)
    this.ticketService.getNombreTiketsEnattenteDeconfirmation() .subscribe(data4=>{
      console.log(data2+"id")
    
    this.st4=data4
      console.log(data4)
 
  this.canvas = document.getElementById('myChart');
  this.ctx = this.canvas.getContext('2d');
  let myChart = new Chart(this.ctx, {
    type: 'pie',
    data: {
        labels: ["Tickets En attente", "Tickets En cour", "Tickets fermé","Tickets  En attente de confirmation"],
        datasets: [{
            label: 'statistique des statuts',
            data: [data,this.st3,this.st2,this.st4],
            backgroundColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                '#fae7da' 
            ],
            borderWidth: 1
        }]
    },
    options: {
      responsive: false,
   
    }
  });
})
})
})
})
 }

}
