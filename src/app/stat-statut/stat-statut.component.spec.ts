import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatStatutComponent } from './stat-statut.component';

describe('StatStatutComponent', () => {
  let component: StatStatutComponent;
  let fixture: ComponentFixture<StatStatutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatStatutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatStatutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
