import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { TicketServiceService } from '../ticket-service';
import * as Chart from 'chart.js';
@Component({
  selector: 'app-stat-label',
  templateUrl: './stat-label.component.html',
  styleUrls: ['./stat-label.component.css']
})
export class StatLabelComponent implements OnInit   {
  canvas: any;
  st:any ;
  st2:any;
  st3:any;
  st4:any;
  ctx: any;
 constructor(private TicketServiceService: TicketServiceService  ){}
 barChartOptions: ChartOptions = {
  responsive: true,
};

barChartLabels: Label[]
barChartType: ChartType
barChartLegend = true;
barChartPlugins = [];

barChartData: ChartDataSets[]
 ngOnInit(){
  this.TicketServiceService.getNombreTicketsLabelRh().subscribe(res => {
    console.log(res);
 
    this.TicketServiceService.getNombreTicketsLabelMission().subscribe(res2 => {
      console.log(res2);
      this.TicketServiceService.getNombreTicketsLabelFacturation().subscribe(res3 => {
        console.log(res3);
 this. barChartOptions= {
    responsive: true,
  };

 this. barChartLabels = ['Rh', 'Mission', 'Facturation'];
 this. barChartType = 'bar';
 this. barChartLegend = true;
  this.barChartPlugins = [];

 this. barChartData = [
    { data: [res, res2, res3], label: 'Statistiques des labels' }
  ];
})
})
})
}
ngAfterViewInit() {
  this.TicketServiceService.getNombreTicketsLabelRh() .subscribe(data=>{
   console.log(data+"id")
 
 
   console.log(data)
   this.TicketServiceService.getNombreTicketsLabelMission() .subscribe(data2=>{
     console.log(data2+"id")
   
 
     console.log(data2)
     this.TicketServiceService.getNombreTicketsLabelFacturation() .subscribe(data3=>{
       
     
   
       console.log(data3)
     
  
   this.canvas = document.getElementById('myChar');
   this.ctx = this.canvas.getContext('2d');
   let myChart = new Chart(this.ctx, {
     type: 'bar',
     data: {
         labels: ["Rh", "Mission", "Facturation"],
         datasets: [{
             label: 'statistique des labels',
             data: [data,data2,data3],
             backgroundColor: [
                 'rgba(255, 99, 132, 1)',
                 'rgba(255, 99, 132, 1)',
                 'rgba(255, 99, 132, 1)',
                 '#fae7da' 
             ],
             borderWidth: 1
         }]
     },
     options: {
       responsive: false,
    
     }
   });
 })
 })
 })

  }

}
